﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Node : MonoBehaviour
{
    [Header("Communication nodes")] public List<Node> ribsNodes = new List<Node>();

    public LineRenderer lineRenderer;
    
    [HideInInspector] public float weightNode = -1;
    [HideInInspector] public bool isAplly = false;

    [HideInInspector] public Node previousNode;
    [HideInInspector] public int numberToNode= 0;

    public void CalculateRibsNode()
    {
        if(ribsNodes.Count == 0 )
            return ;
   
        for (int i = 0; i < ribsNodes.Count; i++)
        {
            if (ribsNodes[i] == null)
            {
                ribsNodes.RemoveAt(i);
                continue;
            }

            if (this == ribsNodes[i])
            {
                ribsNodes.RemoveAt(i);
                continue;
            }

            List<Node> linkCheckNode = ribsNodes[i].ribsNodes;

            bool isThisNode = false;
            int indexRepNode = 0;

            for (int j = 0; j < linkCheckNode.Count; j++)
            {

                if (this == linkCheckNode[j])
                {
                    if (indexRepNode == 1)
                    {
                        linkCheckNode.RemoveAt(j);
                        continue;
                    }

                    isThisNode = true;
                    indexRepNode++;
                }
            }

            if (!isThisNode)
            {
                linkCheckNode.Add(this);
            }
            
        }

        int countLine = ribsNodes.Count * 2;
        
        lineRenderer.positionCount = countLine;

        int index = 0;
        
        for (int i = 0; i < ribsNodes.Count; i++)
        {
            lineRenderer.SetPosition(i+index,transform.position);
            lineRenderer.SetPosition((i+1) + index,ribsNodes[i].transform.position);
            index ++ ;
        }

        isAplly = false;
        weightNode = -1;
    }
    
#if UNITY_EDITOR

    private void OnDrawGizmos()
    {
        CalculateRibsNode();      
    }
    
#endif
}