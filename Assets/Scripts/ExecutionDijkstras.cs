﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ExecutionDijkstras : MonoBehaviour
{
    [Header("Starting top graph")] public Node nodeStart;
    [Header("End top graph")] public Node nodeEnd;
    public LineRenderer lineRenderer;
    public Button path;
    private List<Node> nodes = new List<Node>();


    private void Start()
    {
        nodes.AddRange(GetComponentsInChildren<Node>());

        for (int i = 0; i < nodes.Count; i++)
        {
            nodes[i].CalculateRibsNode();
        }
        
        path.onClick.AddListener(delegate { BuildingPathButton(nodeStart,nodeEnd,nodes); });
    }
    
    private void RenderPath(List<Vector3> path,LineRenderer lineRenderer)
    {
        lineRenderer.positionCount = path.Count;

        for (int i = 0; i < path.Count; i++)
        {
            path[i] =  lineRenderer.transform.InverseTransformVector(path[i]);
        }

        lineRenderer.SetPositions(path.ToArray());
    }
    
    private void BuildingPathButton(Node startNode,Node endNode,List<Node> allNode)
    {
        List<Vector3> path = GetLittlePath(nodeStart, nodeEnd, nodes);

        if (path == null)
        {
            return;
        }
        
        RenderPath(path,lineRenderer);
    }
    
    private List<Vector3> GetLittlePath(Node startNode,Node endNode,List<Node> allNode)
    {
        if (startNode.ribsNodes.Count == 0 || allNode.Count == 0)
            return null;
        
        nodeStart.weightNode = 0;
        
        SetWeightRibs(startNode);

        while (true)
        {
            Node tempNode = MinWeightNode(allNode);

            if (tempNode == null)
            {
                break;
            }
            
            SetWeightRibs(tempNode);           
        }

        if (endNode.weightNode < 0)
        {
            Debug.Log("No Way");
            return null;
        }

        List<Vector3> tempPath = new List<Vector3>();

        tempPath.Add(endNode.transform.position);
        
        Node node = endNode.previousNode;
        
        for (int i = endNode.numberToNode; i > 0 ; i--)
        {       
            tempPath.Add(node.transform.position);

            node = node.previousNode;

        }
        
        return tempPath;

    }
    
    private void SetWeightRibs(Node node)
    {    
        if(node.isAplly)
            return;
        
        for (int i = 0; i < node.ribsNodes.Count; i++)
        {
            if(node.ribsNodes[i].isAplly)
                continue;
            
            float tempWeight = GetRibWeight(node, node.ribsNodes[i].transform.position);
            
            if (node.ribsNodes[i].weightNode < 0)
            {
                SetNodeInfo(node.ribsNodes[i],tempWeight,node);
            }
            else if (tempWeight < node.ribsNodes[i].weightNode)
            {           
                SetNodeInfo(node.ribsNodes[i],tempWeight,node);
            }
                
        }

        node.isAplly = true;
    }
    
    private Node MinWeightNode(List<Node> allNodes)
    {
        float minWeight = -1;

        Node tempMinNode = null;
        
        for (int i = 0; i < allNodes.Count; i++)
        {
            if(allNodes[i].isAplly || allNodes[i].weightNode < 0)
                continue;

            if (allNodes[i].weightNode < minWeight || minWeight < 0)
            {
                minWeight = allNodes[i].weightNode;
                tempMinNode = allNodes[i];
            }
        }

        return tempMinNode;
    }
    
    private void SetNodeInfo(Node nodeRibs,float tempWeight, Node node)
    {
        nodeRibs.weightNode = tempWeight;
        nodeRibs.previousNode = node;
        nodeRibs.numberToNode = node.numberToNode + 1;
    }
    
    private float GetRibWeight(Node startNode,Vector3 endNode)
    {
        return (startNode.weightNode + Vector3.Distance(startNode.transform.position, endNode));
    }    
    
}